GRANT ALL PRIVILEGES ON *.* TO 'user'@'%' IDENTIFIED BY 'secret';
GRANT ALL PRIVILEGES ON *.* TO 'user'@'localhost' IDENTIFIED BY 'secret';
FLUSH PRIVILEGES;

create database if not exists `price_tier_schemas`;

USE `price_tier_schemas`;

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `price_tier_schemas`;
CREATE TABLE `price_tier_schemas` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `account_id` int(11) NOT NULL,
    `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
    `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '0 = Incomplete, 1 = Active, 2 = Inactive',
    `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = False, >=1 = True',
    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`)
);


DROP TABLE IF EXISTS `price_tiers`;
CREATE TABLE `price_tiers` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `price_tier_schema_id` int(11) NOT NULL,
    `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
    `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0= exact ,1= Range',
    `min` float(15,4) NOT NULL DEFAULT '0' COMMENT 'in case of exact, min and max will be same',
    `max` float(15,4) NOT NULL DEFAULT '-1' COMMENT 'in case of exact, min and max will be same',  
    `increment` float(15,4) NOT NULL DEFAULT '0.0000' COMMENT 'in case of exact, increment will be 0',
    `display_order` int(11) NOT NULL COMMENT 'maintain order of tier',
    `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = False, >=1 = True',
    `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
    `deleted_at` datetime DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `price_tier_schema_id` (`price_tier_schema_id`),
    CONSTRAINT `price_tier_schemas_price_tiers_id_fk` FOREIGN KEY (`price_tier_schema_id`) REFERENCES `price_tier_schemas` (`id`)
);

DROP TRIGGER IF EXISTS price_tiers_on_delete_trigger;
CREATE TRIGGER price_tiers_on_delete_trigger
    BEFORE UPDATE 
    ON price_tiers
    FOR EACH ROW 
BEGIN
    IF (NEW.deleted_at IS NULL) THEN SET NEW.deleted = 0; ELSE SET NEW.deleted = OLD.id; END IF;
END;


DROP TRIGGER  IF EXISTS price_tier_schemas_on_delete_trigger;
CREATE TRIGGER price_tier_schemas_on_delete_trigger 
    BEFORE UPDATE 
    ON price_tier_schemas
    FOR EACH ROW 
BEGIN
    IF (NEW.deleted_at IS NULL) THEN SET NEW.deleted = 0; ELSE SET NEW.deleted = OLD.id; END IF;
END;

