const dotenv = require('dotenv');

dotenv.config();

const config = {
  ENVIRONMENT: process.env.ENVIRONMENT || 'DEVELOPMENT',
  MICROSERVICE_NAME: process.env.MICROSERVICE_NAME || 'NODE_BOILERPLATE',
  MICROSERVICE_IP: process.env.MICROSERVICE_IP || '0.0.0.0',
  APP_HOST: process.env.APP_HOST || '0.0.0.0',
  APP_PORT: process.env.APP_PORT || '8102',
  SWAGGER_PORT: process.env.SWAGGER_PORT || '8102',
  MYSQL_HOST: process.env.MYSQL_HOST || 'localhost',
  MYSQL_USERNAME: process.env.MYSQL_USERNAME || 'root',
  MYSQL_PASSWORD: process.env.MYSQL_PASSWORD || 'root',
  MYSQL_DB_NAME: process.env.MYSQL_DB_NAME || 'price_tier_schemas',
};

module.exports = config;
