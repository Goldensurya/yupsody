const successUtils = require("../utils/success");
const errorUtils = require("../utils/error");
const getId = require("../validations/get-id.validation");
const priceTierSchemasService = require("../services/price-tier-schemas.service");

/******************************  PRICE TIER SCHEMS *************************************************** */

const findAllNyop = async (req, res, next) => {
  // const Id = req.params.priceTierSchemaId;

  try {
    // const id = await getId.validate(Id);
    const data = await priceTierSchemasService.findAllNyop(req.query);
    if (data.length > 0) {
      return successUtils.handler({ price_tier_schemas: data }, req, res);
    }
  } catch (err) {
    return next(err);
  }
};

const findSingleNyop = async (req, res, next) => {
  try {
    const priceTier = await priceTierSchemasService.findSingleNyop(req.params.priceTierSchemaId);
    console.log("********** priceTier ***********",priceTier);
    
    return successUtils.handler({ price_tier: priceTier[0] }, req, res);
  } catch (err) {
    return next(err);
  }
};

const addNyopSchema = async (req, res, next) => {
  try {
    const data = await priceTierSchemasService.addNyopSchema(
      req.body,
      req.headers["account-id"]
    );
    return successUtils.handler({ price_tier_schema: data[0] }, req, res);
  } catch (err) {
    return next(err);
  }
};

const updateNyopSchema = async (req, res, next) => {
  try {
    // const Id = req.params.priceTierSchemaId;
    const data = await priceTierSchemasService.updateNyopSchema(
      req.body,
      req.headers["account-id"],
      req.params
    );
    return successUtils.handler({ price_tier_schema: data[0] }, req, res);
  } catch (err) {
    return next(err);
  }
};

const deleteNyopSchema = async (req, res, next) => {
  try {
  
    const getdata = await priceTierSchemasService.findSingleNyop(req.params.priceTierSchemaId);
    console.log("*******Getdata*****", getdata);

    if (getdata.length > 0) {
      const deleteData = await priceTierSchemasService.deleteNyopSchema(req.params.priceTierSchemaId);
      return successUtils.handler({ price_tier_schema: getdata }, req, res);
    }

    // console.log("********data*******", deleteData);

    // if (deleteData == undefined || deleteData == null) {
    //   res.status(404).json({
    //     error: {
    //       code: 404,
    //       message: "Not Found"
    //     }
    //   });
    // } else {
    // }
  } catch (err) {
    return next(err);
  }
};

const nyopCount = async (req, res, next) => {
  try {
    console.log("hiiiiiiiiiiiii");

    const data = await priceTierSchemasService.nyopCount(req.query);
    return successUtils.handler(data[0], req, res);
  } catch (err) {
    return next(err);
  }
};

/************************ PRICE TIER  ************************************** */

const getAllPriceTier = async (req, res, next) => {
  try {
    const id = await getId.validate(req.params.priceTierSchemaId);
    const priceTier = await priceTierSchemasService.getAllPriceTier(
      id,
      req.query
    );
    return successUtils.handler(priceTier, req, res);
  } catch (err) {
    return next(err);
  }
};

const getOnePriceTier = async (req, res, next) => {
  try {
    const priceTierId = await getId.validate(req.params.priceTierId);
    const priceTierSchemaId = await getId.validate(
      req.params.priceTierSchemaId
    );
    const data = {
      priceTierSchemaId: priceTierSchemaId,
      priceTierId: priceTierId
    };

    const priceTier = await priceTierSchemasService.getOnePriceTier(data);
    return successUtils.handler({ price_tier: priceTier[0] }, req, res);
  } catch (err) {
    return next(err);
  }
};

const addPriceTier = async (req, res, next) => {
  try {
    const body = req.body;
    if (body.type == 0 && body.min == body.max) {
      res.send({
        error: {
          code: 400,
          message: "Min and Max should be different"
        }
      });
    } else if (body.type == 1 && body.min > body.max) {
      res.send({
        error: {
          code: 400,
          message: "Max is less than or equal to min value"
        }
      });
    } else if (body.type == 0 && body.min > body.max) {
      res.send({
        error: {
          code: 400,
          message: "Max is less than or equal to min value and type should be 1"
        }
      });
    } else if (body.type == 1 && body.min == body.max) {
      res.send({
        error: {
          code: 400,
          message: "Min and Max should be different and type should be 0"
        }
      });
    } else {
      const price_tiers = await priceTierSchemasService.addPriceTier(
        req.body,
        req.params
      );
      return successUtils.handler({ price_tiers }, req, res);
    }
  } catch (err) {
    return next(err);
  }
};

const updatePriceTier = async (req, res, next) => {
  try {
    const body = req.body;
    if (body.type == 0 && body.min == body.max) {
      res.send({
        error: {
          code: 400,
          message: "Min and Max should be different"
        }
      });
    } else if (body.type == 1 && body.min > body.max) {
      res.send({
        error: {
          code: 400,
          message: "Max is less than or equal to min value"
        }
      });
    } else if (body.type == 0 && body.min > body.max) {
      res.send({
        error: {
          code: 400,
          message: "Max is less than or equal to min value and type should be 1"
        }
      });
    } else if (body.type == 1 && body.min == body.max) {
      res.send({
        error: {
          code: 400,
          message: "Min and Max should be different and type should be 0"
        }
      });
    } else {
      const price_tier = await priceTierSchemasService.updatePriceTier(
        req.body,
        req.params
      );
      return successUtils.handler({ price_tier }, req, res);
    }
  } catch (err) {
    return next(err);
  }
};

const deletePriceTier = async (req, res, next) => {
  try {
    const priceTierId = await getId.validate(req.params.priceTierId);
    const priceTierSchemaId = await getId.validate(
      req.params.priceTierSchemaId
    );
    const data = {
      priceTierSchemaId: priceTierSchemaId,
      priceTierId: priceTierId
    };
    const getData = await priceTierSchemasService.getOnePriceTier(data);
    console.log("******getData*********", getData);

    if (getData.length > 0) {
      const priceTier = await priceTierSchemasService.deletePriceTier(data);
      return successUtils.handler(getData, req, res);
    }

    // console.log('asdfsadf', priceTier)
    // if (priceTier == undefined || priceTier == null) {
    //   res.status(404).json({
    //     error: {
    //       code: 404,
    //       message: "Not Found"
    //     }
    //   });
    // } else {

    // }
  } catch (err) {
    return next(err);
  }
};

module.exports = {
  findAllNyop,
  findSingleNyop,
  addNyopSchema,
  updateNyopSchema,
  deleteNyopSchema,
  nyopCount,
  getAllPriceTier,
  getOnePriceTier,
  addPriceTier,
  updatePriceTier,
  deletePriceTier
};
