const doc = {
  swagger: '2.0',
  info: {
    version: '1.0.0',
    title: 'Yapsody Node.JS NYOP MS',
  },
  host: '0.0.0.0:8102',
  basePath: '/',
  schemes: ['http', 'https'],
  consumes: ['application/json'],
  produces: ['application/json'],
  tags: [
  ],

  paths: {
    '/price-tier-schemas': {
      get: {
        tags: ['Price Tier Schema'],
        summary: 'Get all NYOP',
        description: 'This will return a list of nyop.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'page_no',
            in: 'query',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            default: 1,
          },
          {
            name: 'page_size',
            in: 'query',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            default: 100,
          },
          {
            name: 'sort_by',
            in: 'query',
            type: 'string',
            pattern: '[a-z]',
            default: 'created_at',
          },
          {
            name: 'sort_order',
            in: 'query',
            schema: {
              type: 'string',
              minLength: 3,
              maxLength: 4,
              pattern: '[a-z]',
              enum: ['asc', 'desc'],
              default: 'desc',
            },
          },
          {
            name: 'status',
            in: 'query',
            type: 'integer',
            enum: [0, 1, 2],
          },
          {
            name: 'search',
            in: 'query',
            type: 'string',
            minLength: 3,
            maxLength: 255,
          },
          {
            name: 'ids',
            in: 'query',
            type: 'string',
            minLength: 0,
            maxLength: 255,
          },
        ],
        responses: {
          200: {
            description: 'Get List',
            schema: {
              $ref: '#/schemas/NYOPSchemaList',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },

      post: {
        tags: ['Price Tier Schema'],
        summary: 'Create NYOP schema',
        description: 'This api returns a new nyop with provided body.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'body',
            in: 'body',
            schema: {
              $ref: '#/schemas/AddNYOPSchema',
            },
          },
        ],
        responses: {
          200: {
            description: 'Get One',
            schema: {
              $ref: '#/schemas/NYOPSchema',
            },
            meta: {
              $ref: '#/definitions/meta',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
            meta: {
              $ref: '#/definitions/meta',
            },
          },
          404: {
            schema: {
              $ref: '#/definitions/404',
            },
          },
          412: {
            schema: {
              $ref: '#/definitions/412',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },
    },

    '/price-tier-schemas/count': {
      get: {
        summary: 'Get count of nyop schemas.',
        tags: ['Price Tier Schema'],
        description: 'This will return count of nyop schemas.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'status',
            in: 'query',
            type: 'integer',
            enum: [0, 1, 2],
          },
          {
            name: 'search',
            in: 'query',
            type: 'string',
            minLength: 3,
            maxLength: 255,
          },
        ],
        responses: {
          200: {
            description: 'Get Count',
            schema: {
              type: 'object',
              properties: {
                data: {
                  type: 'object',
                  properties: {
                    count: {
                      type: 'number',
                      example: 10,
                    },
                  },
                },
                meta: {
                  $ref: '#/definitions/meta',
                },
              },
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },
    },

    '/price-tier-schemas/{priceTierSchemaId}': {
      get: {
        tags: ['Price Tier Schema'],
        summary: 'Get one nyop schema by id.',
        description: 'This will Return nyop schema with provided id.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            in: 'path',
            name: 'priceTierSchemaId',
            type: 'number',
            required: true,
          },
        ],
        responses: {
          200: {
            description: 'Get One',
            schema: {
              $ref: '#/schemas/NYOPSchema',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          404: {
            schema: {
              $ref: '#/definitions/404',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },

      put: {
        tags: ['Price Tier Schema'],
        summary: 'Update nyop schema.',
        description: 'This api returns updated nyop with provided id after updating with provided body.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            in: 'path',
            name: 'priceTierSchemaId',
            type: 'number',
            required: true,
          },
          {
            name: 'body',
            in: 'body',
            schema: {
              $ref: '#/schemas/UpdateNYOPSchema',
            },
          },
        ],
        responses: {
          200: {
            description: 'Get One',
            schema: {
              $ref: '#/schemas/NYOPSchema',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          404: {
            schema: {
              $ref: '#/definitions/404',
            },
          },
          412: {
            schema: {
              $ref: '#/definitions/412',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },

      delete: {
        tags: ['Price Tier Schema'],
        summary: 'Delete nyop schema.',
        description: 'This will return a list of nyop schemas.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            in: 'path',
            name: 'priceTierSchemaId',
            type: 'number',
            required: true,
          },
        ],
        responses: {
          200: {
            description: 'Get One',
            schema: {
              $ref: '#/schemas/NYOPSchema',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          404: {
            schema: {
              $ref: '#/definitions/404',
            },
          },
          412: {
            schema: {
              $ref: '#/definitions/412',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },
    },

    '/price-tier-schemas/{priceTierSchemaId}/price-tiers': {
      get: {
        tags: ['Price Tiers'],
        summary: 'Get all price tiers',
        description: 'This will return a list of nyop.',
        parameters: [
          {
            name: 'priceTierSchemaId',
            in: 'path',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true
          },
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'page_no',
            in: 'query',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            default: 1,
          },
          {
            name: 'page_size',
            in: 'query',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            default: 100,
          },
          {
            name: 'sort_by',
            in: 'query',
            type: 'string',
            pattern: '[a-z]',
            default: 'created_at',
          },
          {
            name: 'sort_order',
            in: 'query',
            schema: {
              type: 'string',
              minLength: 3,
              maxLength: 4,
              pattern: '[a-z]',
              enum: ['asc', 'desc'],
              default: 'desc',
            },
          },
          {
            name: 'status',
            in: 'query',
            type: 'integer',
            enum: [0, 1, 2],
          },
          {
            name: 'search',
            in: 'query',
            type: 'string',
            minLength: 3,
            maxLength: 255,
          },
          {
            name: 'ids',
            in: 'query',
            type: 'string',
            minLength: 0,
            maxLength: 255,
          },
        ],
        responses: {
          200: {
            description: 'Get List',
            schema: {
              $ref: '#/schemas/GetPriceTierList',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },

      post: {
        tags: ['Price Tiers'],
        summary: 'Create price tier',
        description: 'This api returns a new price tier with provided body.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            in: 'path',
            name: 'priceTierSchemaId',
            type: 'number',
            required: true,
          },
          {
            name: 'body',
            in: 'body',
            schema: {
              $ref: '#/schemas/AddNYOPSchemaPriceTier',
            },
          },
        ],
        responses: {
          200: {
            description: 'Get One',
            schema: {
              $ref: '#/schemas/NYOPSchemaPriceTier',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          404: {
            schema: {
              $ref: '#/definitions/404',
            },
          },
          412: {
            schema: {
              $ref: '#/definitions/412',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },
    },

    '/price-tier-schemas/{priceTierSchemaId}/price-tiers/{priceTierId}': {
      get: {
        tags: ['Price Tiers'],
        summary: 'Get one price tier by id.',
        description: 'This will Return price tier with provided id.',
        parameters: [
          // {
          //   name: 'priceTierSchemaId',
          //   in: 'path',
          //   type: 'integer',
          //   minimum: 10,
          //   exclusiveMinimum: true,
          //   required: true
          // },
          // {
          //   name: 'priceTierId',
          //   in: 'path',
          //   type: 'integer',
          //   minimum: 10,
          //   exclusiveMinimum: true,
          //   required: true
          // },
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            in: 'path',
            name: 'priceTierSchemaId',
            type: 'number',
            required: true,
          },
          {
            in: 'path',
            name: 'priceTierId',
            type: 'number',
            required: true,
          },
        ],
        responses: {
          200: {
            description: 'Get One',
            schema: {
              $ref: '#/schemas/NYOPSchemaPriceTier',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          404: {
            schema: {
              $ref: '#/definitions/404',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },

      put: {
        tags: ['Price Tiers'],
        summary: 'Update price tier.',
        description: 'This api returns updated price tier with provided id after updating with provided body.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            in: 'path',
            name: 'priceTierSchemaId',
            type: 'number',
            required: true,
          },
          {
            in: 'path',
            name: 'priceTierId',
            type: 'number',
            required: true,
          },
          {
            name: 'body',
            in: 'body',
            schema: {
              $ref: '#/schemas/UpdateNYOPSchemaPriceTier',
            },
          },
        ],
        responses: {
          200: {
            description: 'Get One',
            schema: {
              $ref: '#/schemas/NYOPSchemaPriceTier',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          404: {
            schema: {
              $ref: '#/definitions/404',
            },
          },
          412: {
            schema: {
              $ref: '#/definitions/412',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },

      delete: {
        tags: ['Price Tiers'],
        summary: 'Delete price tier.',
        description: 'This will return deleted price tier.',
        parameters: [
          {
            name: 'whitelabel-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'account-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            name: 'user-id',
            in: 'header',
            type: 'integer',
            minimum: 1,
            exclusiveMinimum: true,
            required: true,
            default: 1,
          },
          {
            in: 'path',
            name: 'priceTierSchemaId',
            type: 'number',
            required: true,
          },
          {
            in: 'path',
            name: 'priceTierId',
            type: 'number',
            required: true,
          },
        ],
        responses: {
          200: {
            description: 'Get One',
            schema: {
              $ref: '#/schemas/NYOPSchemaPriceTier',
            },
          },
          400: {
            schema: {
              $ref: '#/definitions/400',
            },
          },
          404: {
            schema: {
              $ref: '#/definitions/404',
            },
          },
          412: {
            schema: {
              $ref: '#/definitions/412',
            },
          },
          500: {
            schema: {
              $ref: '#/definitions/500',
            },
          },
        },
      },
    },
  },

  schemas: {
    NYOPSchema: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            price_tier_schema: {
              type: 'object',
              properties: {
                id: {
                  type: 'number',
                },
                account_id: {
                  type: 'number',
                },
                name: {
                  type: 'string',
                },
                description: {
                  type: 'string',
                },
                status: {
                  type: 'number',
                },
                deleted: {
                  type: 'number',
                },
                created_at: {
                  type: 'string',
                  format: 'date-time',
                },
                updated_at: {
                  type: 'string',
                  format: 'date-time',
                },
                deleted_at: {
                  type: 'string',
                  format: 'date-time',
                },
              },
            },
          },
        },
        meta: {
          $ref: '#/definitions/meta',
        },
      },
    },

    NYOPSchemaList: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            price_tier_schemas: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: {
                    type: 'number',
                  },
                  account_id: {
                    type: 'number',
                  },
                  name: {
                    type: 'string',
                  },
                  description: {
                    type: 'string',
                  },
                  status: {
                    type: 'number',
                  },
                  deleted: {
                    type: 'number',
                  },
                  created_at: {
                    type: 'string',
                    format: 'date-time',
                  },
                  updated_at: {
                    type: 'string',
                    format: 'date-time',
                  },
                  deleted_at: {
                    type: 'string',
                    format: 'date-time',
                  },
                },
              },
            },
          },
        },
        meta: {
          $ref: '#/definitions/meta',
        },
      },
    },

    AddNYOPSchema: {
      type: 'object',
      require: ['name'],
      properties: {
        name: {
          type: 'string',
        },
        description: {
          type: 'string',
        },
        status: {
          type: 'number',
        },
      },
    },

    UpdateNYOPSchema: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
        },
        description: {
          type: 'string',
        },
        status: {
          type: 'number',
        },
      },
    },

    GetPriceTierList: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            price_tiers: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  id: {
                    type: 'number',
                  },
                  price_tier_schema_id: {
                    type: 'number',
                  },
                  name: {
                    type: 'string',
                  },
                  description: {
                    type: 'string',
                  },
                  type: {
                    type: 'number',
                  },
                  min: {
                    type: 'number',
                    format: 'float',
                    multipleOf: '00000000000000.0001',
                  },
                  max: {
                    type: 'number',
                    format: 'float',
                    multipleOf: '00000000000000.0001',
                  },
                  increment: {
                    type: 'number',
                    format: 'float',
                    multipleOf: '00000000000000.0001',
                  },
                  display_order: {
                    type: 'number',
                  },
                  status: {
                    type: 'number',
                  },
                  deleted: {
                    type: 'number',
                  },
                  created_at: {
                    type: 'string',
                    format: 'date-time',
                  },
                  updated_at: {
                    type: 'string',
                    format: 'date-time',
                  },
                  deleted_at: {
                    type: 'string',
                    format: 'date-time',
                  },
                },
              },
            },
          },
        },
        meta: {
          $ref: '#/definitions/meta',
        },
      },
    },

    NYOPSchemaPriceTier: {
      type: 'object',
      properties: {
        data: {
          type: 'object',
          properties: {
            price_tier: {
              type: 'object',
              properties: {
                id: {
                  type: 'number',
                },
                price_tier_schema_id: {
                  type: 'number',
                },
                name: {
                  type: 'string',
                },
                description: {
                  type: 'string',
                },
                type: {
                  type: 'number',
                },
                min: {
                  type: 'number',
                  format: 'float',
                  multipleOf: '00000000000000.0001',
                },
                max: {
                  type: 'number',
                  format: 'float',
                  multipleOf: '00000000000000.0001',
                },
                increment: {
                  type: 'number',
                  format: 'float',
                  multipleOf: '00000000000000.0001',
                },
                display_order: {
                  type: 'number',
                },
                status: {
                  type: 'number',
                },
                deleted: {
                  type: 'number',
                },
                created_at: {
                  type: 'string',
                  format: 'date-time',
                },
                updated_at: {
                  type: 'string',
                  format: 'date-time',
                },
                deleted_at: {
                  type: 'string',
                  format: 'date-time',
                },
              },
            },
          },
        },
        meta: {
          $ref: '#/definitions/meta',
        },
      },
    },

    AddNYOPSchemaPriceTier: {
      type: 'object',
      require: ['name'],
      properties: {
        name: {
          type: 'string',
        },
        description: {
          type: 'string',
        },
        type: {
          type: 'number',
        },
        min: {
          type: 'number',
          format: 'float',
          multipleOf: '00000000000000.0001',
          default: 0,
        },
        max: {
          type: 'number',
          format: 'float',
          multipleOf: '00000000000000.0001',
          default: -1,
        },
        increment: {
          type: 'number',
          format: 'float',
          multipleOf: '00000000000000.0001',
        },
        display_order: {
          type: 'number',
        },
        status: {
          type: 'number',
        },
      },
    },

    UpdateNYOPSchemaPriceTier: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
        },
        description: {
          type: 'string',
        },
        type: {
          type: 'number',
        },
        min: {
          type: 'number',
          format: 'float',
          multipleOf: '00000000000000.0001',
        },
        max: {
          type: 'number',
          format: 'float',
          multipleOf: '00000000000000.0001',
        },
        increment: {
          type: 'number',
          format: 'float',
          multipleOf: '00000000000000.0001',
        },
        display_order: {
          type: 'number',
        },
        status: {
          type: 'number',
        },
      },
    },
  },

  definitions: {
    400: {
      description: 'Bad Request',
      type: 'object',
      properties: {
        error: {
          type: 'object',
          properties: {
            code: {
              type: 'integer',
              example: 400,
            },
            message: {
              type: 'string',
              example: 'Bad Request',
            },
            fields: {
              type: 'array',
              items: {
                type: 'object',
                properties: {
                  key: {
                    type: 'string',
                    example: 'name',
                  },
                  type: {
                    type: 'string',
                    example: 'any.required',
                  },
                  message: {
                    type: 'string',
                    example: 'Name is required',
                  },
                },
              },
            },
          },
        },
        meta: {
          $ref: '#/definitions/meta',
        },
      },
    },

    404: {
      description: 'Not Found',
      type: 'object',
      properties: {
        error: {
          type: 'object',
          properties: {
            code: {
              type: 'integer',
              example: 404,
            },
            message: {
              type: 'string',
              example: 'Not Found',
            },
          },
        },
        meta: {
          $ref: '#/definitions/meta',
        },
      },
    },

    412: {
      description: 'Precondition Failed',
      type: 'object',
      properties: {
        error: {
          type: 'object',
          properties: {
            code: {
              type: 'integer',
              example: 412,
            },
            message: {
              type: 'string',
              example: 'Precondition Failed',
            },
          },
        },
        meta: {
          $ref: '#/definitions/meta',
        },
      },
    },

    500: {
      description: 'Internal Server Error',
      type: 'object',
      properties: {
        error: {
          type: 'object',
          properties: {
            code: {
              type: 'integer',
              example: 500,
            },
            message: {
              type: 'string',
              example: 'Internal Server Error',
            },
          },
        },
        meta: {
          $ref: '#/definitions/meta',
        },
      },
    },

    meta: {
      type: 'object',
      properties: {
        version: {
          type: 'number',
          example: '1',
        },
        timestamp: {
          type: 'string',
          example: '2019-08-23T07:11:19.637Z',
        },
      },
    },
  },
};

module.exports = doc;
