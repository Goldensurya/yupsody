const express = require("express");
// const handlers = require('@yapsody/lib-handlers');
const handlers = require("@yapsody/yap-handlers");
var HttpStatus = require("http-status-codes");
const apiRoutes = express.Router();

const noteRoutes = require("./note.routes");

const PriceTierRoute = require("./price-tier-schemas.routes");

apiRoutes.use("/notes", noteRoutes);

apiRoutes.use("/price-tier-schemas", PriceTierRoute);


apiRoutes.use("*", (req, res) =>
  res
    .status(405)
    .send({
      error: {
        code: 405,
        message: "Method Not Allowed"
      }
    })
);

// apiRoutes.use("*", (req, res) =>
//   res
//     .status(handlers.response.STATUS.NOT_FOUND)
//     .send(
//       handlers.response.getErrorPayload(
//         "Not Found",
//         handlers.response.STATUS.NOT_FOUND
//       )
//     )
// );

// apiRoutes.use("*", (req, res) =>
//   response.status(HttpStatus.METHOD_NOT_ALLOWED))
//   .send({
//     error: HttpStatus.getStatusText(HttpStatus.METHOD_NOT_ALLOWED)
// });


module.exports = apiRoutes;
