const express = require('express');
const price_tier_schemas = require('../controllers/price-tier-schemas.controller');
const PriceTierValdation = require('../validations/price-tier-schemas.validation');

const price_tier_schemas_routes = express.Router({});


/******************************  PRICE TIER SCHEMS *************************************************** */

price_tier_schemas_routes.get('/', PriceTierValdation.getAllNyopSchema, price_tier_schemas.findAllNyop);

price_tier_schemas_routes.get('/count',PriceTierValdation.getCountNyopSchema, price_tier_schemas.nyopCount);

price_tier_schemas_routes.get('/:priceTierSchemaId',PriceTierValdation.priceTierSchemaId, price_tier_schemas.findSingleNyop);

price_tier_schemas_routes.post('/',PriceTierValdation.addNyopSchema, price_tier_schemas.addNyopSchema);

price_tier_schemas_routes.put('/:priceTierSchemaId',[PriceTierValdation.priceTierSchemaId,PriceTierValdation.addNyopSchema], price_tier_schemas.updateNyopSchema);

price_tier_schemas_routes.delete('/:priceTierSchemaId',PriceTierValdation.priceTierSchemaId, price_tier_schemas.deleteNyopSchema);



/************************ PRICE TIER  ************************************** */

price_tier_schemas_routes.get('/:priceTierSchemaId/price-tiers',  PriceTierValdation.getAllNyopSchema, price_tier_schemas.getAllPriceTier);

price_tier_schemas_routes.get('/:priceTierSchemaId/price-tiers/:priceTierId',price_tier_schemas.getOnePriceTier);

price_tier_schemas_routes.post('/:priceTierSchemaId/price-tiers',[PriceTierValdation.addPriceTier],price_tier_schemas.addPriceTier);

price_tier_schemas_routes.put('/:priceTierSchemaId/price-tiers/:priceTierId',[PriceTierValdation.paramsValidation,PriceTierValdation.addPriceTier],price_tier_schemas.updatePriceTier);

price_tier_schemas_routes.delete('/:priceTierSchemaId/price-tiers/:priceTierId',price_tier_schemas.deletePriceTier);






module.exports = price_tier_schemas_routes;