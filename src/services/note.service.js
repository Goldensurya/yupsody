// const yapUtils = require('@yapsody/lib-utils');
const yapUtils = require('@yapsody/yap-utils');

const { Op } = require('sequelize');
const { NoteModel } = require('../managers/sequelize.manager');
const { STATUS } = require('../consts');
const { errorUtils } = require('../utils');

const getListCount = async ({
  account_id, status, search,
}) => {
  const where = {
    account_id,
  };

  if (status) {
    where.status = status;
  }

  if (search) {
    where.name = {
      [Op.like]: `%${search}%`,
    };
  }

  return NoteModel.count({
    where,
  });
};

const getList = async ({
  account_id, page_no, page_size, status, sort_by, sort_order, search, ids,
}) => {
  const limit = page_size;
  const offset = (page_no - 1) * limit;

  const where = {
    account_id,
  };

  if (ids) {
    where.id = ids;
  }

  if (status) {
    where.status = status;
  }

  if (search) {
    where.name = {
      [Op.like]: `%${search}%`,
    };
  }

  const order = [];
  order.push([sort_by, sort_order]);

  return NoteModel.findAll({
    where,
    order,
    offset,
    limit,
  }).catch((err) => {
    // eslint-disable-next-line no-console
    console.error(err);
  });
};

const getOne = async ({ account_id, id }) => {
  const where = {
    account_id,
    id,
  };

  const item = await NoteModel.findOne({
    where,
  }).catch((err) => {
    // eslint-disable-next-line no-console
    console.error(err);
  });

  if (!item) {
    return errorUtils.throwNotFound('Note');
  }

  return item;
};

const addOne = async ({ account_id, name, description }) => NoteModel.create({
  account_id,
  name,
  description,
}).catch((err) => {
  // eslint-disable-next-line no-console
  console.error(err);
});

const updateOne = async ({
  account_id, id, name, description,
}) => {
  const item = await getOne({
    account_id,
    id,
  });

  item.name = name !== undefined ? name : item.name;
  item.description = description !== undefined ? description : item.description;

  return item.save();
};

const enableOne = async ({ id, account_id }) => {
  const item = await getOne({
    id,
    account_id,
  });

  if (item.status !== STATUS.DISABLED) {
    throw errorUtils.throwPreconditionFailed('Only disabled note can be enabled');
  }

  item.status = STATUS.ENABLED;
  return item.save();
};

const disableOne = async ({ id, account_id }) => {
  const item = await getOne({
    id,
    account_id,
  });

  if (item.status !== STATUS.ENABLED) {
    throw errorUtils.throwPreconditionFailed('Only enabled note can be disabled');
  }

  item.status = STATUS.DISABLED;
  return item.save();
};

const deleteOne = async ({ account_id, id }) => {
  const item = await getOne({
    account_id,
    id,
  });

  if (item.status === STATUS.ENABLED) {
    return errorUtils.throwPreconditionFailed('Enabled note can\'t be deleted');
  }

  return item.destroy();
};

const suggestCopyName = async ({ account_id, name }) => {
  const where = {
    account_id,
    name: {
      [Op.like]: `${name}%`,
    },
  };

  const items = await NoteModel.findAll({
    where,
  });

  const existingNames = items.map((item) => item.name);

  return yapUtils.string.suggestCopyName({
    name,
    existingNames,
    maxLength: 255,
  });
};

module.exports = {
  getListCount,
  getList,
  getOne,
  addOne,
  updateOne,
  enableOne,
  disableOne,
  deleteOne,
  suggestCopyName,
};
