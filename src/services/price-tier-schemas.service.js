// const yapUtils = require("@yapsody/lib-utils");

const yapUtils = require("@yapsody/yap-utils");

const { Op } = require("sequelize");
const { errorUtils } = require("../utils");
const sequelize = require("../managers/sequelize.manager").sequelize;

/******************************  PRICE TIER SCHEMS *************************************************** */
const findAllNyop = async (data) => {
  getdata = {};
  if(data.status == 1){
    getdata.status = 1;
  }else if(data.status == 2){
    getdata.status = 2;
  } else{
    getdata.status = 0;
  }

  // if(data.sort_by == 'created_at')           {
  //   getdata.sort_by = 'created_at';
  // }else{
  //   getdata.sort_by = data.sort_by
  // }
  if(data.search == undefined){
    getdata.search = null;
  }else{
    getdata.search = data.search;
  }
  console.log('dsafjkds', data.search)
  const item = await sequelize.query("call find_all_nyop(?,?,?)",{
    replacements:[getdata.status,data.sort_by, getdata.search]
  });
  console.log("**************findAllNyop********* ", item);

  if (item.length == 0) {
    return errorUtils.throwNotFound();
  }
  return item;
};

const findSingleNyop = async param => {
  console.log("******** find_single_nyop id ************",param);
  const item = await sequelize.query("call find_single_nyop(?)", {
    replacements: [param]
  });
  console.log("**************findSingleNyop********* ", item);

  if (!item || item.length == 0) {
    return errorUtils.throwNotFound("NYOP");
  }

  return item;
};

const addNyopSchema = async (body, header) => {
  const item = await sequelize.query("call add_nyop_schema(?,?,?,?)", {
    replacements: [header, body.name, body.description, body.status]
  });
  console.log("**************addNyopSchema********* ", item);

  if (!item || item.length == 0 || item == undefined) {
    return errorUtils.throwNotFound("NYOP");
  }
  return item;
};

const updateNyopSchema = async (body, header, params) => {
  console.log("body", body, header);

  const item = await sequelize.query("call update_nyop_schema(?,?,?,?,?)", {
    replacements: [
      header,
      body.name,
      body.description,
      body.status,
      params.priceTierSchemaId
    ]
    // type: sequelize.QueryTypes.UPDATE
  });
  console.log("**************updateNyopSchema********* ", item);

  if (!item || item.length == 0 || item == undefined) {
    return errorUtils.throwNotFound("NYOP");
  }
  return item;
};

const deleteNyopSchema = async data => {
  const item = await sequelize.query("call delete_nyop_schema(?)", {
    replacements: [data],
  });
  console.log("**************deleteNyopSchema********* ", item);
  if (!item || item.length == 0 || item == undefined) {
    return errorUtils.throwNotFound("NO DATA IN DATABASE");
  }
  return item;
};

const nyopCount = async data => {
  var getdata = {};
  if(data.search){
    getdata.search = data.search
  }else{
    getdata.search = '0';
  }
  if(data.status > 0){
    getdata.status = data.status;
  }else {
    getdata.status = 0;
  }

  console.log("*******getdata******",getdata);
  
  const item = await sequelize.query("call get_all_count_nyop(?,?)", {
    replacements:[getdata.status,getdata.search]
  });
  console.log("count in *************** ", item);

  if (item == "" || item == undefined || item == null) {
    return errorUtils.throwNotFound("NYOP");
  }
  return item;
};

/************************ PRICE TIER  ************************************** */

const getAllPriceTier = async (id, data) => {

  getdata = {};
  if(data.status == 1){
    getdata.status = 1;
  }else if(data.status == 2){
    getdata.status = 2;
  } else{
    getdata.status = 0;
  }

  if(data.search == undefined){
    getdata.search = null;
  }else{
    getdata.search = data.search;
  }


  const item = await sequelize.query("call get_all_price_tier(?,?,?,?)", {
    replacements: [id, getdata.status,data.sort_by, getdata.search]
  });
  console.log("**************getAllPriceTier********* ", item);

  if (!item || item.length == 0 || item == undefined) {
    return errorUtils.throwNotFound("price_tiers");
  }

  return item;
};

const getOnePriceTier = async param => {
 
  const item = await sequelize.query("call get_one_price_tier(?,?);", {
    replacements: [param.priceTierSchemaId, param.priceTierId]
  });
  console.log("**************getOnePriceTier********* ", item);

  if (Array.isArray(item) && item.length == 0) {
    return errorUtils.throwNotFound("price_tiers");
  }
  return item;
};

const addPriceTier = async (body, param) => {
  const item = await sequelize.query("call add_price_tier(?,?,?,?,?,?,?,?,?)", {
    replacements: [
      param.priceTierSchemaId,
      body.name,
      body.description,
      body.type,
      body.min,
      body.max,
      body.increment,
      body.display_order,
      body.status
    ]
  });
  console.log("**************addPriceTier********* ", item);

  if (!item || item.length == 0) {
    return errorUtils.throwNotFound("price_tiers");
  }
  return item;
};

const updatePriceTier = async (body, param) => {
  if (body.increment > 0) {
    const item = await sequelize.query(
      "call update_price_tier(?,?,?,?,?,?,?,?,?,?)",
      {
        replacements: [
          param.priceTierId,
          param.priceTierSchemaId,
          body.name,
          body.description,
          body.type,
          body.min,
          body.max,
          body.increment,
          body.display_order,
          body.status
        ]
      }
    );
    console.log("**************updatePriceTier********* ", item);

    if (!item || item.length == 0) {
      return errorUtils.throwNotFound("price_tiers");
    }
    return item;
  } else {
    return errorUtils.throwCustomJoiValidationError('Increment value should be greter than 0','Increment')
    // const err = new Error("Increment value should be greter than 0");
    // err.name = "ValidationError";
    // throw err;
  }
};

const deletePriceTier = async data => {
  const item = await sequelize.query("call delete_price_tier(?,?)", {
    replacements: [data.priceTierId, data.priceTierSchemaId],
    type: sequelize.QueryTypes.DELETE
  });
  console.log("**************deletePriceTier********* ", item);

  if (item == "" || item == undefined || item == null) {
    return errorUtils.throwNotFound("price_tiers");
  }
  return item;
};

module.exports = {
  findAllNyop,
  findSingleNyop,
  addNyopSchema,
  updateNyopSchema,
  deleteNyopSchema,
  nyopCount,
  getAllPriceTier,
  getOnePriceTier,
  addPriceTier,
  updatePriceTier,
  deletePriceTier
};
