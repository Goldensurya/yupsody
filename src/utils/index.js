const successUtils = require('./success');
const errorUtils = require('./error');

module.exports = {
  successUtils,
  errorUtils,
};
