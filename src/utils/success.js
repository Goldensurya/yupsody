// const handlers = require('@yapsody/lib-handlers');
const handlers = require('@yapsody/yap-handlers');


/**
 * Handler success and send appropriate response
 * @param data
 * @param req
 * @param res
 * @returns {*}
 */
// eslint-disable-next-line no-unused-vars
const handler = (data, req, res, next) => res.status(handlers.response.STATUS.OK)
  .send(handlers.response.getSuccessPayload(data));

module.exports = {
  handler,
};
