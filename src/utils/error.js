// const handlers = require('@yapsody/lib-handlers');
const handlers = require('@yapsody/yap-handlers');



/**
 * handle exceptions and send appropriate response
 * @param err
 * @param req
 * @param res
 * @returns {*}
 */
// eslint-disable-next-line no-unused-vars
const handler = (err, req, res, next) => {
  // eslint-disable-next-line no-console
  console.error(err);

  let { name } = err;
  if (err.key) {
    name = err.key;
  }

  switch (name) {
    case 'SyntaxError':
      return res.status(handlers.response.STATUS.BAD_REQUEST)
        .send(handlers.response.getErrorPayload('Bad Request', handlers.response.STATUS.BAD_REQUEST, name));
    case 'ValidationError':
      return res.status(handlers.response.STATUS.BAD_REQUEST)
        .send(handlers.response.getJoiValidationErrorPayload(err, handlers.response.STATUS.BAD_REQUEST, name));
    case 'SequelizeValidationError':
      return res.status(handlers.response.STATUS.BAD_REQUEST)
        .send(handlers.response.getErrorPayload(err, handlers.response.STATUS.BAD_REQUEST, name));
    case 'SequelizeDatabaseError':
      return res.status(handlers.response.STATUS.BAD_REQUEST)
        .send(handlers.response.getErrorPayload(err, handlers.response.STATUS.BAD_REQUEST, name));
    case 'SequelizeUniqueConstraintError':
      return res.status(handlers.response.STATUS.CONFLICT)
        .send(handlers.response.getErrorPayload('Duplicate', handlers.response.STATUS.CONFLICT, name));
    case 'SequelizeForeignKeyConstraintError':
      return res.status(handlers.response.STATUS.CONFLICT)
        .send(handlers.response.getErrorPayload(err, handlers.response.STATUS.CONFLICT, name));
    case 'NotFound':
      return res.status(handlers.response.STATUS.NOT_FOUND)
        .send(handlers.response.getErrorPayload(err, handlers.response.STATUS.NOT_FOUND, name));
    case 'PreconditionFailed':
      return res.status(handlers.response.STATUS.PRECONDITION_FAILED)
        .send(handlers.response.getErrorPayload(err, handlers.response.STATUS.PRECONDITION_FAILED, name));
    default:
      return res.status(handlers.response.STATUS.INTERNAL_SERVER_ERROR)
        .send(handlers.response.getErrorPayload('Internal Server Error', handlers.response.STATUS.INTERNAL_SERVER_ERROR, name));
  }
};

/**
 * Throw not found exception
 * @param itemName
 */
const throwNotFound = (itemName = 'Item') => {
  const err = new Error(`${itemName} Not Found`);
  err.name = 'NotFound';
  throw err;
};

/**
 * Throw precondition failed  exception
 * @param message
 */
const throwPreconditionFailed = (message) => {
  const err = new Error(message);
  err.name = 'PreconditionFailed';
  throw err;
};

/**
 * Throw custom validation error in Joi error format
 * @param message
 * @param field_name
 */
const throwCustomJoiValidationError = (message, field_name) => {
  const err = new Error(message);
  err.name = 'ValidationError';
  err.details = [];
  err.details.push({
    context: { key: field_name },
    type: 'custom',
    message,
  });
  throw err;
};

module.exports = {
  handler,
  throwNotFound,
  throwPreconditionFailed,
  throwCustomJoiValidationError,
};
