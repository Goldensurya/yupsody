const Sequelize = require('sequelize');
const {
  note
} = require('../models');
const config = require('../config');

const sequelize = new Sequelize(config.MYSQL_DB_NAME, config.MYSQL_USERNAME, config.MYSQL_PASSWORD, {
  host: config.MYSQL_HOST,
  dialect: 'mysql',
  logging: false,
  dialectOptions: {
    charset: 'utf8mb4',
  },
  define: {
    underscored: true,
    timestamps: true,
    createdAt: 'created_at',
    updatedAt: 'updated_at',
    deletedAt: 'deleted_at',
    paranoid: true,
  },
});

sequelize.sync({ force: true })
  .then(() => {
    console.log(`Database & tables created!`)
  });



const NoteModel = note(sequelize, Sequelize);


module.exports = {
  sequelize,
  NoteModel,
};
