const Joi = require("@hapi/joi");
// const yapValidations = require("@yapsody/lib-validations");
const yapValidations = require("@yapsody/yap-validations");
/******************************  PRICE TIER SCHEMS *************************************************** */



const addNyopSchema = async (req, res, next) => {

  console.log("********* resuest **********",req);
  
  const schema = Joi.object().keys({
    name: Joi.string().min(3).max(255)
      .required(),
    description: Joi.string().allow([""])
    .max(255)
      // .regex(/^[a-zA-Z0-9]{1,255}$/)
      .required(),
    status: Joi.number().required()
  });
  // await Joi.validate(req.body, schema, (err, value) => {
  //   if (err) {
  //     // res.status(400).json("Error: " + err["details"][0]["message"]);
  //     // console.log("Error: ", err["details"][0]["message"]);
  //     // for
  //     res.status(400).json({
  //       error: {
  //         code: 400,
  //         message: "Bad Request",
  //         fields: [
  //           {
  //             key: err["details"][0]["path"][0],
  //             type: "any.required",
  //             message: err["details"][0]["path"][0] + " is required"
  //           }
  //         ]
  //       }
  //     });
  //   } else {
  //     next();
  //   }
  // });
  await Joi.validate(req.body, schema, (err, value) => {
    if (err) {
      console.log(err);
      res.status(400).json({
        error: {
          code: 400,
          message: "Bad Request",
          fields: [
            {
              key: err["details"][0]["path"][0],
              type: "any.required",
              message: err["details"][0]["message"]
            }
          ]
        }
      });
    } else {
      next();
    }
  });
};


const getAllNyopSchema = async (req, res, next) => {
  const reqData = { ...req.query };
  console.log("reqData",reqData);
  
  const schema = Joi.object().keys({
    page_no: Joi.number().min(1).default(1),
    page_size: Joi.number().min(1).default(100),
    sort_by:Joi.string().allow(['',null]),
    sort_order:Joi.string().allow(['',null]),
    status:Joi.number().integer().min(0).max(2).allow([null]),
    search:Joi.string().min(3).allow(['',null]),
    ids:Joi.string().allow(['',null])
  });
  
  
  await Joi.validate(reqData, schema, (err, value) => {
    if (err) {
      console.log(err);
      res.status(400).json({
        error: {
          code: 400,
          message: "Bad Request",
          fields: [
            {
              key: err["details"][0]["path"][0],
              type: "any.required",
              message: err["details"][0]["message"]
            }
          ]
        }
      });
    } else {
      next();
    }
  });
};


const getCountNyopSchema = async (req, res, next) => {
  const reqData = { ...req.query };
  console.log("******reqData************",reqData);
  
  const schema = Joi.object().keys({
    status:Joi.number().integer().min(0).max(2).allow([null]),
    search:Joi.string()
  });
  
  
  await Joi.validate(reqData, schema, (err, value) => {
    if (err) {
      console.log(err);
      res.status(400).json({
        error: {
          code: 400,
          message: "Bad Request",
          fields: [
            {
              key: err["details"][0]["path"][0],
              type: "any.required",
              message: err["details"][0]["message"]
            }
          ]
        }
      });
    } else {
      next();
    }
  });
};


const priceTierSchemaId = async (req, res, next) => {
  const schema = Joi.object().keys({
    priceTierSchemaId: Joi.number().integer().required()
  });
  await Joi.validate(req.params, schema, (err, value) => {
    if (err) {
      console.log(err);
      res.status(400).json({
        error: {
          code: 400,
          message: "Bad Request",
          fields: [
            {
              key: err["details"][0]["path"][0],
              type: "any.required",
              message: err["details"][0]["message"]
            }
          ]
        }
      });
    } else {
      next();
    }
  });
};




/************************ PRICE TIER  ************************************** */

const addPriceTier = async (req, res, next) => {
  const schema = Joi.object().keys({
    name: Joi.string().min(3).max(255).optional()
    .allow([""]),
    description: Joi.string().optional().allow([""])
    .max(255),
    type: Joi.number()
      .min(0)
      .max(1).default(0).allow([null,0]),
      // .required(),
    min: Joi.number()
      .min(0)
      .required(),
    max: Joi.number()
      .min(0)
      .required(),
    increment: Joi.number()
      .min(0)
      .default()
      .required(),
    display_order: Joi.number().required(),
    status: Joi.number().min(0).max(2).required()
  });
  await Joi.validate(req.body, schema, (err, value) => {
    if (err) {
      console.log(err);
      res.status(400).json({
        error: {
          code: 400,
          message: "Bad Request",
          fields: [
            {
              key: err["details"][0]["path"][0],
              type: "any.required",
              message: err["details"][0]["message"]
            }
          ]
        }
      });
    } else {
      next();
    }
  });
};

const updatePriceTier = async (req, res, next) => {
  const schema = Joi.object().keys({
    // name: Joi.string()
    //   .regex(/^[a-zA-Z]{1,255}$/)
    //   .required(),
    // description: Joi.string()
    //   .regex(/^[a-zA-Z]{1,255}$/)
    //   .required(),
    name: Joi.string().min(3).max(255).optional()
    .allow([""]),
    description: Joi.string().allow([""])
    .max(255),
    type: Joi.number()
      .max(1)
      .required(),
    min: Joi.number().required(),
    max: Joi.number().required(),
    increment: Joi.number().required(),
    display_order: Joi.number().required(),
    status: Joi.number().min(0).max(2).required()
  });
  await Joi.validate(req.body, schema, (err, value) => {
    if (err) {
      res.status(400).json({
        error: {
          code: 400,
          message: "Bad Request",
          fields: [
            {
              key: err["details"][0]["path"][0],
              type: "any.required",
              message: err["details"][0]["path"][0] + " is required"
            }
          ]
        }
      });
    } else {
      next();
    }
  });
};

const paramsValidation = async (req, res, next) => {
  const schema = Joi.object().keys({
    priceTierSchemaId: Joi.number().integer().required(),
    priceTierId: Joi.number().integer().required()
  });
  await Joi.validate(req.params, schema, (err, value) => {
    if (err) {
      console.log(err);
      res.status(400).json({
        error: {
          code: 400,
          message: "Bad Request",
          fields: [
            {
              key: err["details"][0]["path"][0],
              type: "any.required",
              message: err["details"][0]["message"]
            }
          ]
        }
      });
    } else {
      next();
    }
  });
};


module.exports = {
  addNyopSchema,
  priceTierSchemaId,
  getAllNyopSchema,
  getCountNyopSchema,
  paramsValidation,
  addPriceTier,
  updatePriceTier
};

// module.exports = Joi.object().keys({
//   name: yapValidations.common.name.required().label("Name"),
//   description: yapValidations.common.description.label("Description"),
//   type: Joi.number().required(),
//   min: Joi.number().required(),
//   max: Joi.number().required(),
//   increment: Joi.number().required(),
//   display_order: Joi.number().required(),
//   status: Joi.number().required()
// });
