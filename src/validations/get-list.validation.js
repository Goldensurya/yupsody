const Joi = require('@hapi/joi');
// const yapValidations = require('@yapsody/lib-validations');
const yapValidations = require('@yapsody/yap-validations');


module.exports = Joi.object()
  .keys({
    page_no: yapValidations.page.number.default(1).label('Page Number'),
    page_size: yapValidations.page.size.default(100).label('Page Size'),
    sort_by: yapValidations.sort.by.default('created_at').label('Sort By'),
    sort_order: yapValidations.sort.order.default('desc').label('Sort Order'),
    status: yapValidations.common.status.label('Status'),
    search: yapValidations.common.searchQuery.label('Search Query'),
    ids: Joi.array().items(yapValidations.common.id).label('Ids'),
  })
  .unknown(true);
