const Joi = require('@hapi/joi');
// const yapValidations = require('@yapsody/lib-validations');
const yapValidations = require('@yapsody/yap-validations');


module.exports = Joi.object()
  .keys({
    name: yapValidations
      .common
      .name
      .label('Name'),
    description: yapValidations
      .common
      .description
      .label('Description'),
  }).min(1);
