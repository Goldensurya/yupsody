-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 29, 2019 at 11:44 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `price_tier_schemas`
--
CREATE DATABASE IF NOT EXISTS `price_tier_schemas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `price_tier_schemas`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `add_nyop_schema`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_nyop_schema` (IN `_account_id` INT(10), IN `_name` VARCHAR(255), IN `_description` VARCHAR(255), IN `_status` TINYINT(1))  BEGIN

SET @last_insert_id = null;


INSERT INTO  price_tier_schemas(account_id,name,description,status) VALUES (_account_id,_name,_description,_status);
 
SELECT LAST_INSERT_ID() INTO @last_insert_id;

SELECT * FROM price_tier_schemas WHERE price_tier_schemas.id = @last_insert_id;
END$$

DROP PROCEDURE IF EXISTS `add_price_tier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_price_tier` (IN `_priceTierSchemaId` INT(10), IN `_name` VARCHAR(255), IN `_description` VARCHAR(255), IN `_type` TINYINT(1), IN `_min` FLOAT(15,4), IN `_max` FLOAT(15,4), IN `_increment` FLOAT(15,4), IN `_display_order` TINYINT(1), IN `_status` TINYINT(1))  BEGIN

SET @last_inserted_id = null;

INSERT 
INTO 
price_tiers (price_tiers.price_tier_schema_id,price_tiers.name,price_tiers.description,price_tiers.type,price_tiers.min,price_tiers.max,price_tiers.increment,price_tiers.display_order,price_tiers.status)
VALUES 
(_priceTierSchemaId,_name,_description,_type,_min,_max,_increment,_display_order,_status);

SELECT LAST_INSERT_ID() INTO @last_inserted_id;

SELECT * FROM price_tiers WHERE price_tiers.id = @last_inserted_id;

END$$

DROP PROCEDURE IF EXISTS `delete_nyop_schema`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_nyop_schema` (IN `_priceTierSchemaId` INT(10))  BEGIN
SET @_priceTierSchemaId = 0;

SELECT p.id INTO @_priceTierSchemaId FROM price_tier_schemas p WHERE p.id = _priceTierSchemaId;

IF @_priceTierSchemaId > 0 THEN
	
DELETE FROM price_tier_schemas WHERE price_tier_schemas.id = _priceTierSchemaId;
   
SELECT @_priceTierSchemaId AS priceTierSchemaId;

END IF;
    


END$$

DROP PROCEDURE IF EXISTS `delete_price_tier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_price_tier` (IN `_priceTierId` INT(10), IN `_priceTierSchemaId` INT(10))  BEGIN

SET @id= null;

SELECT id INTO @id FROM price_tiers WHERE price_tiers.id = _priceTierId AND price_tiers.price_tier_schema_id = _priceTierSchemaId;


IF(@id > 0) THEN

DELETE FROM price_tiers WHERE price_tiers.id = _priceTierId AND price_tiers.price_tier_schema_id = _priceTierSchemaId;

SELECT @id AS priceTierId;


END IF;

END$$

DROP PROCEDURE IF EXISTS `find_all_nyop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_all_nyop` (IN `_status` TINYINT(1), IN `_sort_by` VARCHAR(10), IN `_search` VARCHAR(25))  BEGIN

SET @null_result = null;

SELECT ISNULL(_search) INTO @null_result;

IF(_status > 0 AND @null_result = 0) 

THEN
    
SELECT * FROM price_tier_schemas p WHERE p.status = _status AND p.name = _search ORDER BY _sort_by ASC ;
 
ELSEIF (_status > 0 AND @null_result = 1) THEN
 
SELECT * FROM price_tier_schemas p WHERE p.status = _status ORDER BY _sort_by ASC ;
 
ELSEIF(_status = 0 AND @null_result = 0) 

THEN
    
SELECT * FROM price_tier_schemas p WHERE p.status = _status AND p.name = _search ORDER BY _sort_by ASC ;
 
ELSE

SELECT * FROM price_tier_schemas ORDER BY _sort_by ASC;

END IF;

END$$

DROP PROCEDURE IF EXISTS `find_single_nyop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `find_single_nyop` (IN `_id` INT(10))  BEGIN

SELECT * FROM price_tier_schemas where price_tier_schemas.id = _id;

END$$

DROP PROCEDURE IF EXISTS `get_all_count_nyop`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_count_nyop` (IN `_status` INT(10), IN `_search` VARCHAR(255))  BEGIN

IF _status > 0 AND _search NOT LIKE '0' THEN

SELECT COUNT(price_tier_schemas.id) AS count FROM price_tier_schemas WHERE price_tier_schemas.status = _status AND
price_tier_schemas.name =  _search;

ELSEIF _status > 0 AND _search LIKE '0' THEN

SELECT COUNT(price_tier_schemas.id) AS count FROM price_tier_schemas WHERE price_tier_schemas.status = _status;


ELSE

SELECT COUNT(price_tier_schemas.id) AS count FROM price_tier_schemas;

END IF;
END$$

DROP PROCEDURE IF EXISTS `get_all_price_tier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_price_tier` (IN `_id` INT(10), IN `_status` TINYINT(1), IN `_sort_by` VARCHAR(25), IN `_search` VARCHAR(25))  BEGIN



SET @null_result = null;

SELECT ISNULL(_search) INTO @null_result;

IF(_status > 0 AND @null_result = 0) 

THEN

    
SELECT * FROM price_tiers p WHERE p.status = _status AND p.name = _search ORDER BY _sort_by ASC ;
 
ELSEIF (_status > 0 AND @null_result = 1) THEN
 
SELECT * FROM price_tiers p WHERE p.status = _status ORDER BY _sort_by ASC ;
 
ELSEIF(_status = 0 AND @null_result = 0) 

THEN
    
SELECT * FROM price_tiers p WHERE p.status = _status AND p.name = _search ORDER BY _sort_by ASC ;
 
ELSE

SELECT * FROM price_tiers ORDER BY _sort_by ASC;

END IF;

END$$

DROP PROCEDURE IF EXISTS `get_one_price_tier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_one_price_tier` (IN `_price_tier_schema_id` INT(10), IN `_id` INT(10))  BEGIN

SELECT * FROM price_tiers WHERE price_tiers.id = _id AND price_tiers.price_tier_schema_id = _price_tier_schema_id;

END$$

DROP PROCEDURE IF EXISTS `update_nyop_schema`$$
CREATE DEFINER=`root`@`%` PROCEDURE `update_nyop_schema` (IN `_account_id` INT(10), IN `_name` VARCHAR(255), IN `_description` VARCHAR(255), IN `_status` TINYINT(1), IN `_id` INT(10))  BEGIN

SET @account_id = 0;

SET @_id = 0;

SELECT p.id,p.account_id INTO  @_id, @account_id FROM price_tier_schemas p WHERE p.id  = _id AND p.account_id = _account_id;

IF @_id > 0 && @account_id > 0 THEN

UPDATE price_tier_schemas SET name = _name,description = _description,status = _status WHERE id = _id AND price_tier_schemas.account_id = _account_id;

SELECT * FROM price_tier_schemas WHERE price_tier_schemas.id = _id;

END IF;

END$$

DROP PROCEDURE IF EXISTS `update_price_tier`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_price_tier` (IN `_priceTierId` INT(10), IN `_priceTierSchemaId` INT(10), IN `_name` VARCHAR(255), IN `_description` VARCHAR(255), IN `_type` TINYINT(1), IN `_min` FLOAT(15,4), IN `_max` FLOAT(15,4), IN `_increment` FLOAT(15,4), IN `_display_order` TINYINT(1), IN `_status` TINYINT(1))  BEGIN


UPDATE
price_tiers p
SET
p.name = _name,
p.description= _description,
p.type= _type,
p.min= _min,
p.max= _max,
p.increment= _increment,
p.display_order= _display_order,
p.status = _status,
p.updated_at = NOW()
WHERE 
p.id = _priceTierId AND p.price_tier_schema_id =  _priceTierSchemaId;

SELECT * FROM price_tiers p WHERE 
p.id = _priceTierId AND p.price_tier_schema_id =  _priceTierSchemaId;


END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `price_tiers`
--

DROP TABLE IF EXISTS `price_tiers`;
CREATE TABLE `price_tiers` (
  `id` int(11) NOT NULL,
  `price_tier_schema_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0= exact ,1= Range',
  `min` float(15,4) NOT NULL DEFAULT '0.0000' COMMENT 'in case of exact, min and max will be same',
  `max` float(15,4) NOT NULL DEFAULT '-1.0000' COMMENT 'in case of exact, min and max will be same',
  `increment` float(15,4) NOT NULL DEFAULT '0.0000' COMMENT 'in case of exact, increment will be 0',
  `display_order` int(11) NOT NULL COMMENT 'maintain order of tier',
  `status` int(10) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = False, >=1 = True',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `price_tiers`
--
DROP TRIGGER IF EXISTS `price_tiers_on_delete_trigger`;
DELIMITER $$
CREATE TRIGGER `price_tiers_on_delete_trigger` BEFORE UPDATE ON `price_tiers` FOR EACH ROW BEGIN
    IF (NEW.deleted_at IS NULL) THEN SET NEW.deleted = 0; ELSE SET NEW.deleted = OLD.id; END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `price_tier_schemas`
--

DROP TABLE IF EXISTS `price_tier_schemas`;
CREATE TABLE `price_tier_schemas` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '0 = Incomplete, 1 = Active, 2 = Inactive',
  `deleted` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = False, >=1 = True',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `price_tier_schemas`
--
DROP TRIGGER IF EXISTS `price_tier_schemas_on_delete_trigger`;
DELIMITER $$
CREATE TRIGGER `price_tier_schemas_on_delete_trigger` BEFORE UPDATE ON `price_tier_schemas` FOR EACH ROW BEGIN
    IF (NEW.deleted_at IS NULL) THEN SET NEW.deleted = 0; ELSE SET NEW.deleted = OLD.id; END IF;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `price_tiers`
--
ALTER TABLE `price_tiers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `price_tier_schema_id` (`price_tier_schema_id`);

--
-- Indexes for table `price_tier_schemas`
--
ALTER TABLE `price_tier_schemas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `price_tiers`
--
ALTER TABLE `price_tiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `price_tier_schemas`
--
ALTER TABLE `price_tier_schemas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `price_tiers`
--
ALTER TABLE `price_tiers`
  ADD CONSTRAINT `price_tier_schemas_price_tiers_id_fk` FOREIGN KEY (`price_tier_schema_id`) REFERENCES `price_tier_schemas` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
