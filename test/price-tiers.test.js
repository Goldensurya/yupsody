/* eslint-disable no-unused-vars */
const { Op } = require('sequelize');
const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../src/app');
const { priceTierSchemaModel, priceTierModel } = require('../src/managers/sequelize.manager');

chai.use(chaiHttp);

// eslint-disable-next-line no-unused-vars
const should = chai.should();

const headers = {
  'whitelabel-id': 1,
  'account-id': 1,
  'user-id': 1,
};

const gt255 = `This website stores cookies on your computer. 
These cookies are used to collect information about how you interact with our website and 
allow us to remember you.We use this information in order to improve and customize your 
browsing experience and for analytics and metrics about our visitors both on this website and other media`;

let baseUrl;
let nyopSchema = { id: 1 };

function randomString(length) {
  let text = '';
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

const randomDigit = () => Math.floor(100000 + Math.random() * 900000);

const getOnePriceTierSchema = async ({ status, account_id = 1 }) => priceTierSchemaModel.findOne({
  where: {
    account_id,
    status,
  },
});

const getOne = async ({ status, account_id = 1 }) => priceTierModel.findOne({
  where: {
    account_id,
    status,
    schema_id: nyopSchema.id,
  },
});

const getPriceTierSchemaList = async ({ status, account_id = 1, limit = 10 }) => priceTierSchemaModel.findAll({
  where: {
    account_id,
    status,
  },
  limit,
});


const getList = async ({ status, account_id = 1, limit = 10 }) => priceTierModel.findAll({
  where: {
    account_id,
    status,
    schema_id: nyopSchema.id,
  },
  limit,
});

const getDeletedOnePriceTierSchema = async () => priceTierSchemaModel.findOne({
  where: {
    account_id: 1,
    deleted_at: {
      [Op.ne]: null,
    },
  },
  paranoid: false,
});

const getLastRowIdOfPriceTierSchema = async () => priceTierSchemaModel.max('id');

const getDeletedOne = async () => priceTierModel.findOne({
  where: {
    account_id: 1,
    deleted_at: {
      [Op.ne]: null,
    },
  },
  paranoid: false,
});

const createBulkData = async () => {
  await priceTierSchemaModel.bulkCreate([{
    name: randomString(10),
    description: randomString(100),
    status: 2,
  }, {
    name: randomString(10),
    description: randomString(100),
    status: 2,
  }, {
    name: randomString(10),
    description: randomString(100),
    status: 1,
  }]);
  await priceTierModel.bulkCreate([{
    name: randomString(10),
    type: 0,
    min: randomDigit(),
    max: randomDigit(),
    display_order: 1,
    status: 2,
  }, {
    name: randomString(10),
    type: 0,
    min: randomDigit(),
    max: randomDigit(),
    display_order: 1,
    status: 2,
  }, {
    name: randomString(10),
    type: 0,
    min: randomDigit(),
    max: randomDigit(),
    display_order: 1,
    status: 1,
  }]);
};

const deleteSampleData = async () => {
  const priceTierSchema = await getOnePriceTierSchema({ status: 2 });
  const priceTier = await getOne({ status: 2 });
  await priceTierSchemaModel.destroy({
    where: {
      id: priceTierSchema.id,
    },
  });
  await priceTierModel.destroy({
    where: {
      id: priceTier.id,
    },
  });
};

createBulkData();
deleteSampleData();
getOnePriceTierSchema({ status: 2 })
  .then((priceTierSchema) => {
    nyopSchema = priceTierSchema;
    baseUrl = `/price-tier-schemas/${priceTierSchema.id}/price-tiers`;
  });

describe('Price Tiers Test Suit', () => {
  describe(`POST ${baseUrl}`, () => {
    it('TC044 - should create new price tier with name as alphabetic characters', async () => {
      const price = randomDigit();
      const body = {
        name: randomString(10),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC045 - should create new price tier with name as numerical characters', async () => {
      const price = randomDigit();
      const body = {
        name: randomDigit(),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC046 - should create new price tier with name as blank', async () => {
      const price = randomDigit();
      const body = {
        name: '',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC047 - should create new price tier with name as special characters', async () => {
      const price = randomDigit();
      const body = {
        name: '^$^%$^&@@##$',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC048 - should create new price tier with name as Foreign language characters', async () => {
      const price = randomDigit();
      const body = {
        name: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC049 - should create new price tier with duplicate name', async () => {
      const price = randomDigit();
      const priceTier = await getOne({ status: 2 });
      const body = {
        name: priceTier.name,
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    // it('TC044 - should create new price tier with name as alphabetic characters', async () => {
    //   const price = randomDigit();
    //   const body = {
    //     name: randomString(10),
    //     type: 0,
    //     min: price,
    //     max: price,
    //     display_order: 1,
    //   }
    //   const res = await chai.request(app)
    //     .post(`${baseUrl}`)
    //     .set(headers)
    //     .send(body);
    //   res.should.have.status(200);
    //   res.body.should.be.a('object');
    //   res.body.should.have.property('data')
    //     .which.is.an('object')
    //     .that.has.property('price_tiers')
    //     .which.is.an('object')
    //     .that.has.property('name')
    //     .which.is.equal(body.name);
    // });

    // it('TC045 - should create new price tier with name as numerical characters', async () => {
    //   const price = randomDigit();
    //   const body = {
    //     name: randomDigit(),
    //     type: 0,
    //     min: price,
    //     max: price,
    //     display_order: 1,
    //   }
    //   const res = await chai.request(app)
    //     .post(`${baseUrl}`)
    //     .set(headers)
    //     .send(body);
    //   res.should.have.status(200);
    //   res.body.should.be.a('object');
    //   res.body.should.have.property('data')
    //     .which.is.an('object')
    //     .that.has.property('price_tiers')
    //     .which.is.an('object')
    //     .that.has.property('name')
    //     .which.is.equal(body.name);
    // });

    // it('TC046 - should create new price tier with name as blank', async () => {
    //   const price = randomDigit();
    //   const body = {
    //     name: '',
    //     type: 0,
    //     min: price,
    //     max: price,
    //     display_order: 1,
    //   }
    //   const res = await chai.request(app)
    //     .post(`${baseUrl}`)
    //     .set(headers)
    //     .send(body);
    //   res.should.have.status(200);
    //   res.body.should.be.a('object');
    //   res.body.should.have.property('data')
    //     .which.is.an('object')
    //     .that.has.property('price_tiers')
    //     .which.is.an('object')
    //     .that.has.property('name')
    //     .which.is.equal(body.name);
    // });

    // it('TC047 - should create new price tier with name as special characters', async () => {
    //   const price = randomDigit();
    //   const body = {
    //     name: '^$^%$^&@@##$',
    //     type: 0,
    //     min: price,
    //     max: price,
    //     display_order: 1,
    //   }
    //   const res = await chai.request(app)
    //     .post(`${baseUrl}`)
    //     .set(headers)
    //     .send(body);
    //   res.should.have.status(200);
    //   res.body.should.be.a('object');
    //   res.body.should.have.property('data')
    //     .which.is.an('object')
    //     .that.has.property('price_tiers')
    //     .which.is.an('object')
    //     .that.has.property('name')
    //     .which.is.equal(body.name);
    // });

    // it('TC048 - should create new price tier with name as Foreign language characters', async () => {
    //   const price = randomDigit();
    //   const body = {
    //     name: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
    //     type: 0,
    //     min: price,
    //     max: price,
    //     display_order: 1,
    //   }
    //   const res = await chai.request(app)
    //     .post(`${baseUrl}`)
    //     .set(headers)
    //     .send(body);
    //   res.should.have.status(200);
    //   res.body.should.be.a('object');
    //   res.body.should.have.property('data')
    //     .which.is.an('object')
    //     .that.has.property('price_tiers')
    //     .which.is.an('object')
    //     .that.has.property('name')
    //     .which.is.equal(body.name);
    // });

    // it('TC049 - should create new price tier with duplicate name', async () => {
    //   const price = randomDigit();
    //   const priceTier = await getOne({ status: 2 });
    //   const body = {
    //     name: priceTier.name,
    //     type: 0,
    //     min: price,
    //     max: price,
    //     display_order: 1,
    //   }
    //   const res = await chai.request(app)
    //     .post(`${baseUrl}`)
    //     .set(headers)
    //     .send(body);
    //   res.should.have.status(200);
    //   res.body.should.be.a('object');
    //   res.body.should.have.property('data')
    //     .which.is.an('object')
    //     .that.has.property('price_tiers')
    //     .which.is.an('object')
    //     .that.has.property('name')
    //     .which.is.equal(body.name);
    // });

    it('TC050 - should create new price tier with description as alphabetic characters', async () => {
      const price = randomDigit();
      const body = {
        description: randomString(10),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC051 - should create new price tier with description as numerical characters', async () => {
      const price = randomDigit();
      const body = {
        description: randomDigit(),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC052 - should create new price tier with description as blank', async () => {
      const price = randomDigit();
      const body = {
        description: '',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC053 - should create new price tier with description as special characters', async () => {
      const price = randomDigit();
      const body = {
        description: '^$^%$^&@@##$',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC054 - should create new price tier with description as Foreign language characters', async () => {
      const price = randomDigit();
      const body = {
        description: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC055 - should create new price tier with display order as positive number', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('display_order')
        .which.is.equal(body.display_order);
    });

    it('TC056 - should return validation error because display order is string', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: randomString(5),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC057 - should return validation error because display order is negative number', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: -1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC058 - should return validation error because display order is blank', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: '',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC059 - should return validation error because display order is 0 [should start from 1]', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 0,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    // ON HOLD : Discuss this test case Jitendra
    it('TC060', () => {

    });

    it('TC061 - should return error because type is invalid. VALID - [0, 1]', async () => {
      const price = randomDigit();
      const body = {
        type: randomDigit(),
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC062 - should return error because type is string.', async () => {
      const price = randomDigit();
      const body = {
        type: randomString(2),
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC063 - should return error because type is negative number.', async () => {
      const price = randomDigit();
      const body = {
        type: -1,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC064 - should return error because type is blank.', async () => {
      const price = randomDigit();
      const body = {
        type: '',
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC066 - should return error because max and max should be same when type is 1.', async () => {
      const body = {
        type: 1,
        min: 10,
        max: 100,
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC067 - should return error because min and max should be same when type is 0.', async () => {
      const body = {
        type: 0,
        min: randomDigit(),
        max: randomDigit(),
        display_order: 1,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC068', async () => {

    });

    it('TC069 - should create price tier with increments as number', async () => {
      const body = {
        type: 1,
        min: 1,
        max: 10,
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('increment')
        .which.is.equal(body.increment);
    });

    it('TC070 - should return error because increments is string', async () => {
      const body = {
        type: 1,
        min: 1,
        max: 10,
        display_order: 1,
        increment: randomString(2),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC071 - should return error because increments is negative number', async () => {
      const body = {
        type: 1,
        min: 1,
        max: 10,
        display_order: 1,
        increment: -2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC072 - should return error because increments is blank', async () => {
      const body = {
        type: 1,
        min: 1,
        max: 10,
        display_order: 1,
        increment: '',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC074 - should return error because min is string', async () => {
      const body = {
        type: 1,
        min: randomString(2),
        max: 10,
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC075 - should return error because min is negative number', async () => {
      const body = {
        type: 1,
        min: -1,
        max: 10,
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC076 - should return error because min is blank', async () => {
      const body = {
        type: 1,
        min: '',
        max: 10,
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC078 - should return error because max is string', async () => {
      const body = {
        type: 1,
        min: 1,
        max: randomString(2),
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC079 - should return error because max is negative number', async () => {
      const body = {
        type: 1,
        min: 1,
        max: -10,
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC080 - should return error because max is blank', async () => {
      const body = {
        type: 1,
        min: 1,
        max: '',
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC081 - should create price tier with increments as number when type is 0', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
        increment: randomDigit(),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('increment')
        .which.is.equal(body.increment);
    });

    it('TC082 - should return error because increments is string when type is 0', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
        increment: randomString(2),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC083 - should return error because increments is negative number when type is 0', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
        increment: -2,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC084 - should return error because increments is blank when type is 0', async () => {
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
        increment: '',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });
  });

  describe(`TC085 - GET ${baseUrl}/priceTierId`, () => {
    it('J001 - should return the price tier of given id', async () => {
      const priceTier = await getOne({ status: 2 });
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTier.id}`)
        .set(headers);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .which.has.property('price_tier');
    });

    it('J002 - should return validation error because price tier schema id is blank', async () => {
      const priceTier = await getOne({ status: 1 });
      const res = await chai.request(app)
        .get(`/price-tier-schemas//price-tiers/${priceTier.id}`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('J004 - should return 404 NotFound error because price tier schema id does not exist in DB', async () => {
      const lastRowIdOfPriceTierSchema = await getLastRowIdOfPriceTierSchema();
      const res = await chai.request(app)
        .get(`/price-tier-schemas/${Number(lastRowIdOfPriceTierSchema) + 1}/price-tiers/1`)
        .set(headers);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('J005 - should return 404 NotFound error because price tier schema id is deleted', async () => {
      const deletedPriceTierSchema = await getDeletedOnePriceTierSchema();
      const res = await chai.request(app)
        .get(`/price-tier-schemas/${deletedPriceTierSchema.id}/price-tiers/1`)
        .set(headers);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('J006 - should return 400 Validation error because price tier schema id is alphabetic characters', async () => {
      const deletedPriceTierSchemaId = randomString(2);
      const res = await chai.request(app)
        .get(`/price-tier-schemas/${deletedPriceTierSchemaId}/price-tiers/1`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('J007 - should give 400 validation error because price tier schema id can not be a decimal value.', async () => {
      const res = await chai.request(app)
        .get('/price-tier-schemas/22.11/price-tiers/1')
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('J010 - should return 404 NotFound error because price tier id does not exist in DB', async () => {
      const priceTierId = randomDigit();
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierId}`)
        .set(headers);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('J011 - should return 404 NotFound error because price tier id is deleted', async () => {
      const priceTierId = getDeletedOne();
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierId}`)
        .set(headers);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('J012 - should return 400 Validation error because price tier id is alphabetic characters', async () => {
      const priceTierId = randomString(2);
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierId}`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('J013 - should return 400 Validation error because price tier id is decimal value', async () => {
      const priceTierId = 22.11;
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierId}`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });
  });

  describe(`PUT ${baseUrl}/priceTierId`, () => {
    it('TC088 - should update price tier', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('type')
        .which.is.equal(body.type);
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('min')
        .which.is.equal(body.min);
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('max')
        .which.is.equal(body.max);
    });

    it('TC089 - should return 400 Validation error because price tier schema id is blank', async () => {
      const priceTier = await getOne({ status: 2 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`/price-tier-schemas//price-tiers/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC090 - should return 400 Validation error because price tier schema id is alphabetic characters', async () => {
      const priceTier = await getOne({ status: 2 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`/price-tier-schemas/abc/price-tiers/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC091 - should return 400 Validation error because price tier schema id is decimal value', async () => {
      const priceTier = await getOne({ status: 2 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`/price-tier-schemas/22.11/price-tiers/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC092 - should update price tier name and description with price tier schema status as 2', async () => {
      const priceTier = await getOne({ status: 2 });
      const price = randomDigit();
      const body = {
        name: randomString(10),
        description: randomString(100),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC093 - should return 404 NotFound error because price tier schema id does not exist in DB.', async () => {
      const price = randomDigit();
      const nyopSchemaId = randomDigit();
      const body = {
        name: randomString(10),
        description: randomString(100),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`/price-tier-schemas/${nyopSchemaId}/1`)
        .set(headers)
        .send(body);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC094 - should return 404 NotFound error because price tier schema id is deleted.', async () => {
      const price = randomDigit();
      const nyopSchemaId = await getDeletedOnePriceTierSchema();
      const body = {
        name: randomString(10),
        description: randomString(100),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`/price-tier-schemas/${nyopSchemaId}/1`)
        .set(headers)
        .send(body);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC095 - should return 404 Route NotFound error.', async () => {
      const price = randomDigit();
      const body = {
        name: randomString(10),
        description: randomString(100),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/`)
        .set(headers)
        .send(body);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC096 - should return 400 Validation error because price tier id is alphabetic characters.', async () => {
      const price = randomDigit();
      const body = {
        name: randomString(10),
        description: randomString(100),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/abc`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC097 - should return 400 Validation error because price tier id is decimal number.', async () => {
      const price = randomDigit();
      const body = {
        name: randomString(10),
        description: randomString(100),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/22.11`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC099 - should return 404 NotFound error because price tier id is deleted.', async () => {
      const priceTier = await getDeletedOne();
      const price = randomDigit();
      const body = {
        name: randomString(10),
        description: randomString(100),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC100 - should return 404 NotFound error because price tier id does not exist in DB.', async () => {
      const priceTierId = await randomDigit();
      const price = randomDigit();
      const body = {
        name: randomString(10),
        description: randomString(100),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierId}`)
        .set(headers)
        .send(body);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC102 - should update price tier with name as Numerical characters', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        name: randomDigit(),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC103 - should update price tier with name as blank', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        name: '',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC104 - should update price tier with name as special characters', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        name: '$#%&@*&*&^',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC105 - should update price tier with name as Foreign language characters', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        name: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC106 - should update price tier with duplicate name', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        name: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC107 - should update price tier with description as alphabetic characters', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        description: randomString(10),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC108 - should update price tier with description as numerical characters', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        description: randomDigit(),
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC109 - should update price tier with description as blank', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        description: '',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC110 - should update price tier with description as special characters', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        description: '!@#$%^&*',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC111 - should update price tier with description as Foreign language characters', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        description: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
        type: 0,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC113 - should return 400 Validation Error because display order is string', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: randomString(1),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC114 - should return 400 Validation Error because display order is negative number', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: -1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC115 - should return 400 Validation Error because display order is blank', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: '',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it(`TC116 - should return 400 Validation Error because display order is less than 1.
      display order should be greater than or equal to 1.`, async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 0,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    // ON HOLD - Implementation of this test case is confusing
    it('TC117 - should return 412 PreConditionFailed Error because display order is greater than actual price tier present', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: 0,
        min: price,
        max: price,
        display_order: 0,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC118 - should return 400 Validation Error because type can either be 0 or 1.', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: 2,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC119 - should return 400 Validation Error because type is string.', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: randomString(1),
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC120 - should return 400 Validation Error because type is negative number.', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: -1,
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC121 - should return 400 Validation Error because type is blank.', async () => {
      const priceTier = await getOne({ status: 1 });
      const price = randomDigit();
      const body = {
        type: '',
        min: price,
        max: price,
        display_order: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC123 - should update price tier with type as 1.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        type: 1,
        min: 10,
        max: 20,
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('type')
        .which.is.equal(body.type);
    });

    it('TC124 - should return 400 Validation error because min and max should be same when type is 0.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        type: 0,
        min: 10,
        max: 20,
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    // TO DO : check which error it should have throw in this case (400 or 412)
    it('TC125 - should return 400 Validation error because min and max can not be same when type is 1.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        type: 1,
        min: 10,
        max: 10,
        display_order: 1,
        increment: 2,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC126 - should update price tier with min as number when type is 1', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        min: 4,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('min')
        .which.is.equal(body.min);
    });

    it('TC127 - should return Validation error because min is string.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        min: randomString(1),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC128 - should return 400 Validation error because min is negative number', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        min: -10,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC129 - should update price tier with min as blank when type is 1.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        min: '',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC130 - should update price tier with max as number when type is 1', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        max: 30,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('max')
        .which.is.equal(body.max);
    });

    it('TC131 - should return 400 Validation error because max is string.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        max: randomString(1),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC132 - should return 400 Validation error because max is negative number', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        max: -10,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC133 - should update price tier with max as blank when type is 1.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        max: '',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC134 - should update price tier with increments as number when type is 1', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        increment: 4,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('increment')
        .which.is.equal(body.increment);
    });

    it('TC135 - should return 400 Validation error because increments is string.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        increment: randomString(1),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC136 - should return 400 Validation error because increments is negative number', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        increment: -10,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    // TO-DO: Discuss how to send blank value in JSON boody
    it('TC137 - should update price tier with increments as blank with default value 0 when type is 1.', async () => {
      const priceTier = await getOne({ status: 1 });
      const body = {
        increment: '',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTier.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier')
        .which.is.an('object')
        .that.has.property('increment')
        .which.is.equal(0);
    });
  });
});
