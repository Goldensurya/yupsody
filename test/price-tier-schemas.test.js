/* eslint-disable no-unused-vars */
const { Op } = require('sequelize');
const { describe, it } = require('mocha');
const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../src/app');
const { priceTierSchemaModel } = require('../src/managers/sequelize.manager');

chai.use(chaiHttp);

const should = chai.should();

const headers = {
  'whitelabel-id': 1,
  'account-id': 1,
  'user-id': 1,
};

const gt255 = `This website stores cookies on your computer. 
These cookies are used to collect information about how you interact with our website and 
allow us to remember you.We use this information in order to improve and customize your 
browsing experience and for analytics and metrics about our visitors both on this website and other media`;

const baseUrl = '/price-tier-schemas';

function randomString(length) {
  let text = '';
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

const randomDigit = () => Math.floor(100000 + Math.random() * 900000);

const getOne = async ({ status, account_id = 1 }) => priceTierSchemaModel.findOne({
  where: {
    account_id,
    status,
  },
});

const getList = async ({ status, account_id = 1, limit = 10 }) => priceTierSchemaModel.findAll({
  where: {
    account_id,
    status,
  },
  limit,
});


const getDeletedOne = async () => priceTierSchemaModel.findOne({
  where: {
    account_id: 1,
    deleted_at: {
      [Op.ne]: null,
    },
  },
  paranoid: false,
});

const getLastRowId = async () => priceTierSchemaModel.max('id');

const createBulkData = async () => {
  await priceTierSchemaModel.bulkCreate([{
    name: randomString(10),
    description: randomString(100),
    status: 2,
  }, {
    name: randomString(10),
    description: randomString(100),
    status: 2,
  }, {
    name: randomString(10),
    description: randomString(100),
    status: 1,
  }]);
};

const deleteSampleData = async () => {
  const priceTierSchema = await getOne({ status: 2 });
  await priceTierSchemaModel.destroy({
    where: {
      id: priceTierSchema.id,
    },
  });
};

createBulkData();
deleteSampleData();
describe('NYOP Schemas Test Suit', () => {
  describe(`POST ${baseUrl}`, () => {
    let duplicateName;
    it('TC003 - should create price tier schema with name and description', async () => {
      const body = {
        name: randomString(10),
        description: randomString(100),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description');
    });

    it('TC004 - should create price tier schema with name as alphabetic characters', async () => {
      const body = {
        name: randomString(10),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name');
    });

    it('TC005 - should create price tier schema with name as numerical characters ', async () => {
      const body = {
        name: randomDigit(),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.a('string')
        .which.is.equal(body.name);
    });

    it('TC006 - should create price tier schema with name as blank', async () => {
      const body = {
        name: '',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('TC007 - should create price tier schema with name as special characters', async () => {
      const body = {
        name: '^$^%$^&@@##$',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.a('string')
        .which.is.equal(body.name);
    });

    it('TC008 - should create price tier schema with name as Foreign language characters', async () => {
      const body = {
        name: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.a('string')
        .which.is.equal(body.name);
    });

    it('TC009 - should create price tier schema with duplicate value of name', async () => {
      const body = {
        name: duplicateName,
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.a('string')
        .which.is.equal(body.name);
    });

    it('TC010 - should create price tier schema with description as alphabetic characters', async () => {
      const body = {
        name: randomString(10),
        description: randomString(100),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.a('string')
        .which.is.equal(body.description);
    });

    it('TC011 - should create price tier schema with description as numerical characters', async () => {
      const body = {
        name: randomString(10),
        description: randomDigit(),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.a('string')
        .which.is.equal(body.description);
    });

    it('TC012 - should create price tier schema with description as blank', async () => {
      const body = {
        name: randomString(10),
        description: '',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.a('string');
    });

    it('TC013 - should create price tier schema with description as special characters', async () => {
      const body = {
        name: randomString(10),
        description: '^$^%$^&@@##$',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.a('string');
    });

    it('TC014 - should create price tier schema with description as Foreign language characters', async () => {
      const body = {
        name: randomString(10),
        description: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.a('string');
    });

    it('TC015 - should create price tier schema with status as disabled by default', async () => {
      const body = {
        name: randomString(10),
        description: randomString(100),
      };
      const res = await chai.request(app)
        .post(`${baseUrl}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('status')
        .which.is.equal(2);
    });
  });

  describe(`GET ${baseUrl}`, () => {
    it('should return empty list of price tier schemas because status 3 is not available.', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?status=3`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('should return list of display Schemas of given ids', async () => {
      const price_tier_schemas = await getList({ status: 1 });
      const ids = (price_tier_schemas.map((priceTierSchema) => priceTierSchema.id)).join(';');
      const res = await chai.request(app)
        .get(`${baseUrl}?ids=${ids}`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('should give validation error because page_no can not be a garbage value.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?page_no=@#78sdac`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('A002 - should give validation error because page_no can not be 0.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?page_no=0`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('A003 - should return list of price tier schemas [page no = blank]', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?pageno=`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A004 - should give validation error because page_no can not be a string.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?page_no=abc`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('should give validation error because page_size can not be a garbage value.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?page_size=@#78sdac`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('A005 - should give validation error because page_size can not be 0.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?page_size=0`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('A006 - should return list of price tier schemas [page_size = blank]', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?page_size=`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A007 - should give validation error because page_size can not be a string.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?page_size=abc`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('A008 - should return list of price tier schemas because sort_by is null and sort_order asc', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?sort_by=&sort_order=asc`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A009 - should return list of price tier schemas because sort_by is name and sort_order asc', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?sort_by=line1&sort_order=asc`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A010 - should return list of price tier schemas because sort_by is name and sort_order desc', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?sort_by=name&sort_order=desc`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A012 - should return  list of enabled price tier schemas.', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?status=1`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A013 - should return  list of disabled price tier schemas.', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?status=2`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('should give validation error because status can not be more than 2', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?status=6`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('should give validation error because status can not be other than 0, 1 or 2.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?status=167445774435678`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('A014 - should give validation error because status can not be a negative value.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?status=-1`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('A015 - should give validation error because status can not be a decimal value.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?status=22.45`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('A016 - should return list of price tier schemas because search is null', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?search=`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A017 - should return list of price tier schemas which contains line 1', async () => {
      const priceTierSchema = await getOne({ status: 1 });

      const res = await chai.request(app)
        .get(`${baseUrl}?search=${priceTierSchema.name}`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A018 - should return list of price tier schemas which contains 111', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?search=111`) // at least 3 char needed
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schemas')
        .which.is.an('array');
    });

    it('A020 - should give validation error because search is less than 3 characters in length.', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}?search=a`) // at least 3 char needed
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('should give validation error because duplicate parameters are not allowed.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}?sort_by=id&page_no=1&sort_by=id`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });
  });

  describe(`TC016 - GET ${baseUrl}/count`, () => {
    it('B001 - should return count of price tier schemas', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/count`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('count')
        .which.is.an('number');
    });

    it('B002 - should return count of price tier schemas for status 1', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/count?status=1&search=`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('count')
        .which.is.an('number');
    });

    it('B003 - should return count of price tier schemas for status 2 and search empty', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/count?status=2&search=`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('count')
        .which.is.an('number');
    });

    it('should return count of price tier schemas contains 111', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/count?search=111`) // at least 3 char needed
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('count')
        .which.is.an('number');
    });

    it('B004 - should give validation error because status can not be more than 2', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}/count?status=5&search=`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('B005 - should give validation error because status can not be a string value.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}/count?status=abc`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('should return count of price tier schemas for status 1', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/count?status=1`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('count')
        .which.is.an('number');
    });

    it('B006 - should return count of enabled price tier schemas contains [abc] and status 1', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/count?status=1&search=abc`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('count')
        .which.is.an('number');
    });

    it('B007 - should return count of enabled price tier schemas status 2 and search [abc]', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/count?status=2&search=abc`)
        .set(headers);

      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('count')
        .which.is.an('number');
    });

    it('B008 - should give validation error because status can not be a negative value.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}/count?status=-1`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });

    it('B009 - should give validation error because status can not be a decimal value.', async () => {
      const res = await chai
        .request(app)
        .get(`${baseUrl}/count?status=22.45`)
        .set(headers);

      res.should.have.status(400);
      res.error.should.not.be.false;
    });
  });

  describe(`TC017 - POST ${baseUrl}/priceTierSchemaId/clone`, () => {
    it('E002 - should clone inactive price tier schema', async () => {
      const priceTierSchema = await getOne({ status: 2 });
      const res = await chai.request(app)
        .post(`${baseUrl}/${priceTierSchema.id}/clone`)
        .set(headers);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.a('string')
        .which.is.equal(priceTierSchema.name);
      res.body.should.have.property('data')
        .that.has.property('price_tier_schema')
        .that.has.property('description')
        .which.is.a('string')
        .which.is.equal(priceTierSchema.description);
      res.body.should.have.property('data')
        .that.has.property('price_tier_schema')
        .that.has.property('status')
        .which.is.equal(2);
      res.body.should.have.property('data')
        .that.has.property('price_tier_schema')
        .that.has.property('id')
        .which.is.not.equal(priceTierSchema.id);
    });

    it('E003 - should give 404 not found error because data is not exist in DB', async () => {
      const priceTierSchemaId = randomDigit();
      const res = await chai.request(app)
        .post(`${baseUrl}/${priceTierSchemaId}/clone`)
        .set(headers);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('E004 - should give 404 not found error because data is deleted', async () => {
      const priceTierSchema = await getDeletedOne();
      const res = await chai.request(app)
        .post(`${baseUrl}/${priceTierSchema.id}/clone`)
        .set(headers);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('E005 - should give 400 validation error because id is alphabetic characters', async () => {
      const priceTierSchemaId = randomString(5);
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierSchemaId}/clone`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('E006 - should clone active price tier schema', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const res = await chai.request(app)
        .post(`${baseUrl}/${priceTierSchema.id}/clone`)
        .set(headers);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.a('string')
        .which.is.equal(priceTierSchema.name);
      res.body.should.have.property('data')
        .that.has.property('price_tier_schema')
        .that.has.property('description')
        .which.is.a('string')
        .which.is.equal(priceTierSchema.description);
      res.body.should.have.property('data')
        .that.has.property('price_tier_schema')
        .that.has.property('status')
        .which.is.equal(2);
    });

    it('E007 - should give 400 validation error because id is decimal characters', async () => {
      const priceTierSchemaId = 22.14;
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierSchemaId}/clone`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });
  });

  describe(`TC018 - GET ${baseUrl}/priceTierSchemaId`, () => {
    it('F001 - should return one price tier schema of given id.', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .which.has.property('price_tier_schema');
    });

    it('F003 - should return one price tier schema of given id. [ inactive entity ]', async () => {
      const priceTierSchema = await getOne({ status: 2 });
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .which.has.property('price_tier_schema');
    });

    it('F004 - should give 404 not found error because data is not exist in DB', async () => {
      const priceTierSchema = await getDeletedOne();
      const res = await chai.request(app)
        .get(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('F006 - should give 400 validation error because id is alphabetic characters', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/abc`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('F007 - should give 400 validation error because id can not be a decimal value.', async () => {
      const res = await chai.request(app)
        .get(`${baseUrl}/22.45`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });
  });

  describe(`PUT ${baseUrl}/priceTierSchemaId`, () => {
    it('TC020 - should update price tier schema and should return required fields (name, description)', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        name: randomString(10),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description');
    });

    it('TC022 - should update name or description of the specified NYOP schema', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        name: randomString(10),
        description: randomString(100),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC023 - should return 404 error because data is not exist in DB', async () => {
      const priceTierSchemaId = randomDigit();
      const body = {
        name: randomString(10),
        description: randomString(100),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchemaId}`)
        .set(headers)
        .send(body);
      res.should.have.status(404);
      res.error.should.not.be.false;
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC024 - should return 404 error because data is deleted', async () => {
      const priceTierSchema = await getDeletedOne();
      const body = {
        name: randomString(10),
        description: randomString(100),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC025 - should update price tier schema with name as alphabetic characters', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        name: randomString(10),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC026 - should update price tier schema with name as numerical characters', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        name: randomDigit(),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC027 - should return error because name can not be blank', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        name: '',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(412);
      res.error.should.not.be.false;
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC028 - should update price tier schema with name as special characters', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        name: '^$^%$^&@@##$',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC029 - should update price tier schema with name as Foreign language characters', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        name: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC030 - should update price tier schema with duplicate name (same name as other schema name)', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const priceTierSchemaToUpdate = await getOne({ status: 2 });
      const body = {
        name: priceTierSchema.name,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchemaToUpdate.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('name')
        .which.is.equal(body.name);
    });

    it('TC031 - should update price tier schema with description as alphabetic characters', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        description: randomString(10),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC032 - should update price tier schema with description as numerical characters', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        description: randomDigit(),
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC033 - should update price tier schema with description as blank', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        description: '',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC034 - should update price tier schema with description as special characters', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        description: '^$^%$^&@@##$',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC035 - should update price tier schema with description as Foreign language characters', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        description: 'এই এপিআই সরবরাহিত বডি সহ একটি নতুন ডিসপ্লেস্কেমা দেয়।',
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('description')
        .which.is.equal(body.description);
    });

    it('TC036 - should update price tier schema with status as 2 (DISABLED)', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        status: 2,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('status')
        .which.is.equal(body.status);
    });

    it('TC037 - should return error because only enabled NYOP schema can be disabled', async () => {
      const priceTierSchema = await getOne({ status: 2 });
      const body = {
        status: 2,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(412);
      res.error.should.not.be.false;
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC038 - should return error because only disabled NYOP schema can be enabled', async () => {
      const priceTierSchema = await getOne({ status: 1 });
      const body = {
        status: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(412);
      res.error.should.not.be.false;
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('TC039 - should update price tier schema with status as 1 (ENABLED)', async () => {
      const priceTierSchema = await getOne({ status: 2 });
      const body = {
        status: 1,
      };
      const res = await chai.request(app)
        .put(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers)
        .send(body);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .that.has.property('price_tier_schema')
        .which.is.an('object')
        .that.has.property('status')
        .which.is.equal(body.status);
    });
  });

  describe(`TC040 - DELETE ${baseUrl}/priceTierSchemaId`, () => {
    it('G001 - should delete price tier schema', async () => {
      const priceTierSchema = await getOne({ status: 2 });
      const res = await chai.request(app)
        .delete(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .which.has.property('price_tier_schema')
        .that.has.property('deleted_at')
        .which.is.not.equal(null);
    });

    it('should return precondition failed error because price tier schema with status 1 can not be deleted', async () => {
      const priceTierSchema = await getOne({ status: 2 });
      const res = await chai.request(app)
        .delete(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers);
      res.should.have.status(200);
      res.body.should.be.a('object');
      res.body.should.have.property('data')
        .which.is.an('object')
        .which.has.property('price_tier_schema')
        .that.has.property('deleted_at')
        .which.is.not.equal(null);
    });

    it('G004 - should return 404 error because data is not exist in DB', async () => {
      const priceTierSchemaId = randomDigit();
      const res = await chai.request(app)
        .delete(`${baseUrl}/${priceTierSchemaId}`)
        .set(headers);
      res.should.have.status(404);
      res.error.should.not.be.false;
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('G005 - should return 404 error because data is deleted', async () => {
      const priceTierSchema = await getDeletedOne();
      const res = await chai.request(app)
        .delete(`${baseUrl}/${priceTierSchema.id}`)
        .set(headers);
      res.should.have.status(404);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('G006 - should give 400 validation error because id is alphabetic characters', async () => {
      const res = await chai.request(app)
        .delete(`${baseUrl}/abc`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });

    it('G007 - should give 400 validation error because id is decimal characters', async () => {
      const res = await chai.request(app)
        .delete(`${baseUrl}/22.45`)
        .set(headers);
      res.should.have.status(400);
      res.body.should.be.a('object');
      res.body.should.have.property('error')
        .which.is.an('object')
        .which.has.property('message')
        .which.is.an('string');
    });
  });
});
