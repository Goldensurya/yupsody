const priceTierSchema = require('./price-tier-schemas.test');
const priceTier = require('./price-tiers.test');

module.exports = {
  priceTierSchema,
  priceTier,
};
