#!/bin/bash

# DO_NOT_EDIT - Dev Service setup cloudformation script
#aws cloudformation create-stack --stack-name dev-global-lists-stack --template-body file://service-setup.yml --parameters ParameterKey=ServiceName,ParameterValue=global-lists ParameterKey=ServiceDatabaseName,ParameterValue=global_lists ParameterKey=EnvironmentName,ParameterValue=dev ParameterKey=ContainerPort,ParameterValue=8093 ParameterKey=ContainerMemory,ParameterValue=128 ParameterKey=ContainerCpu,ParameterValue=128

# DO_NOT_EDIT - Stage Service setup cloudformation script
#aws cloudformation create-stack --stack-name stage-global-lists-stack --template-body file://service-setup.yml --parameters ParameterKey=ServiceName,ParameterValue=global-lists ParameterKey=ServiceDatabaseName,ParameterValue=global_lists ParameterKey=EnvironmentName,ParameterValue=stage ParameterKey=ContainerPort,ParameterValue=8093 ParameterKey=ContainerMemory,ParameterValue=128 ParameterKey=ContainerCpu,ParameterValue=128

#### Constants
if [ $1 != "dev" ] && [ $1 != "stage" ]
then
  echo "usage: sh deploy.sh [dev|stage] [ECR Path] [NPM_TOKEN]"
  exit 1
fi

DOCKER_NAME=price-tier-schemas
ENV=$1
ECR_PATH=$2
NPM_TOKEN=$3
CLUSTER_NAME=yap-$ENV-cluster
SERVICE_NAME=yap-$ENV-$DOCKER_NAME
AWS_REGION=ap-south-1

#### Functions
docker build . -t $DOCKER_NAME --target prod --build-arg NPM_TOKEN=$NPM_TOKEN
$(aws ecr get-login --no-include-email --region $AWS_REGION)
docker tag $DOCKER_NAME:latest $ECR_PATH/$SERVICE_NAME:latest
docker push $ECR_PATH/$SERVICE_NAME:latest
aws ecs update-service --region $AWS_REGION --cluster $CLUSTER_NAME --service $SERVICE_NAME --desired-count 1 --force-new-deployment
